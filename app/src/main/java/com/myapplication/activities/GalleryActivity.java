package com.myapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.myapplication.R;
import com.myapplication.adapter.GalleryPhotoAdapter;
import com.myapplication.utilities.RecyclerClick;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GalleryActivity extends AppCompatActivity implements RecyclerClick {
    @BindView(R.id.photos_rv)
    RecyclerView photosRV;


    GalleryPhotoAdapter galleryPhotoAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);

        galleryPhotoAdapter = new GalleryPhotoAdapter(GalleryActivity.this, this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(photosRV.getContext(), DividerItemDecoration.VERTICAL);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(GalleryActivity.this, 2);
        photosRV.setLayoutManager(mLayoutManager);
        photosRV.setAdapter(galleryPhotoAdapter);
        galleryPhotoAdapter.notifyDataSetChanged();
        photosRV.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void productClick(View v, int position) {


    }

    @OnClick(R.id.img_view_back)
    public void navBackImg()
    {
        backPress();
    }

    public void backPress() {
        super.onBackPressed();
        finish();
    }

}
