package com.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.myapplication.R;
import com.myapplication.adapter.EditAddressAdapter;
import com.myapplication.utilities.RecyclerClick;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditAddressListActivity extends AppCompatActivity implements RecyclerClick{

    @BindView(R.id.edit_adress_rv)
    RecyclerView editAddressRv;

    private RecyclerView.LayoutManager mLayoutManager;
    EditAddressAdapter allAddressAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address_list);
        ButterKnife.bind(this);
        allAddressAdapter = new EditAddressAdapter(EditAddressListActivity.this,this);
        mLayoutManager = new LinearLayoutManager(EditAddressListActivity.this);
        editAddressRv.setLayoutManager(mLayoutManager);
        editAddressRv.setAdapter(allAddressAdapter);
        allAddressAdapter.notifyDataSetChanged();
    }


    @OnClick(R.id.img_view_back)
    public void navBackImg() {
        backPress();
    }

    public void backPress() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void productClick(View v, int position) {

        startActivity(new Intent(EditAddressListActivity.this,AddAddressActivity.class));
    }
}
