package com.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.myapplication.R;
import com.myapplication.adapter.OrderItemAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReviewOrderActivity extends AppCompatActivity {


    @BindView(R.id.order_rv)
    RecyclerView orderRv;

    private RecyclerView.LayoutManager mLayoutManager;
    OrderItemAdapter orderItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_order);
        ButterKnife.bind(this);

        orderItemAdapter = new OrderItemAdapter(ReviewOrderActivity.this);
        mLayoutManager = new LinearLayoutManager(ReviewOrderActivity.this);
        orderRv.setLayoutManager(mLayoutManager);
        orderRv.setAdapter(orderItemAdapter);
        orderItemAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.img_view_back)
    public void navBackImg() {
        backPress();
    }

    public void backPress() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.proceed_to_check_btn)
    public void proceedToCheckOut() {
        Intent intent = new Intent(ReviewOrderActivity.this, SelectAddressActivity.class);
        startActivity(intent);

    }
}
