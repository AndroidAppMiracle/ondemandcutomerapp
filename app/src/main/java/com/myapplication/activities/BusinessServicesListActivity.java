package com.myapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.myapplication.R;
import com.myapplication.adapter.ServiceRvItemAdapter;
import com.myapplication.utilities.RecyclerClick;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessServicesListActivity extends AppCompatActivity implements RecyclerClick, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.search_img)
    ImageView searchImg;
    @BindView(R.id.swip_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView allServiceRv;
    private RecyclerView.LayoutManager mLayoutManager;
    ServiceRvItemAdapter serviceRvItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_business_service);
        ButterKnife.bind(this);
       // searchImg.setVisibility(View.VISIBLE);
        allServiceRv = (RecyclerView) findViewById(R.id.service_rv);

        serviceRvItemAdapter = new ServiceRvItemAdapter(BusinessServicesListActivity.this, this);
        mLayoutManager = new LinearLayoutManager(BusinessServicesListActivity.this);
        allServiceRv.setLayoutManager(mLayoutManager);
        allServiceRv.setAdapter(serviceRvItemAdapter);
        serviceRvItemAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    public void productClick(View v, int position) {
        Intent intent = new Intent(getApplicationContext(), ServiceDescActivity.class);
        startActivity(intent);
    }

    private void refreshContent() {
      /*  new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        },100);*/
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 100);
    }

    @OnClick(R.id.img_view_back)
    public void navBackImg() {
        backPress();
    }

    public void backPress() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.review_cart_btn)
    public void reviewCart() {
        Intent intent=new Intent(BusinessServicesListActivity.this,ReviewOrderActivity.class);
        startActivity(intent);
    }

}