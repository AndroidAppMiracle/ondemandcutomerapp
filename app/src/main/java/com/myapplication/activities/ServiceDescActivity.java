package com.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.myapplication.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiceDescActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_desc);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.review_cart_btn)
    public void reviewOrderBtn() {
        Intent intent=new Intent(ServiceDescActivity.this,ReviewOrderActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.img_view_back)
    public void navBackImg()
    {
        backPress();
    }

    public void backPress() {
        super.onBackPressed();
        finish();
    }
}
