package com.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.myapplication.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddAddressActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.save_address_btn)
    public void saveAddress() {
        Intent intent = new Intent(AddAddressActivity.this, SelectAddressActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.img_view_back)
    public void navBackImg() {
        backPress();
    }

    public void backPress() {
        super.onBackPressed();
        finish();
    }

}
