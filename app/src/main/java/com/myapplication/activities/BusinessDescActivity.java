package com.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.myapplication.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusinessDescActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_service_desc);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.img_view_back)
    public void navBackImg()
    {
        backPress();
    }

    public void backPress() {
        super.onBackPressed();
        finish();
    }
    @OnClick(R.id.view_services_btn)
    public void viewAllServices()
    {
        Intent intent=new Intent(getApplicationContext(),BusinessServicesListActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.view_gallery_ll)
    public void viewGallery()
    {
        Intent intent=new Intent(getApplicationContext(),GalleryActivity.class);
        startActivity(intent);
    }
}
