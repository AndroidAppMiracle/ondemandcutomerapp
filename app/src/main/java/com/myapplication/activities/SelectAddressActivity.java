package com.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.myapplication.R;
import com.myapplication.adapter.AllAddressAdapter;
import com.myapplication.adapter.OrderItemAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectAddressActivity extends AppCompatActivity {

    @BindView(R.id.all_address_rv)
    RecyclerView allAddressRv;
    @BindView(R.id.edit_img)
    ImageView editImg;
    private RecyclerView.LayoutManager mLayoutManager;
    AllAddressAdapter allAddressAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);
        ButterKnife.bind(this);
        editImg.setVisibility(View.VISIBLE);
        allAddressAdapter = new AllAddressAdapter(SelectAddressActivity.this);
        mLayoutManager = new LinearLayoutManager(SelectAddressActivity.this);
        allAddressRv.setLayoutManager(mLayoutManager);
        allAddressRv.setAdapter(allAddressAdapter);
        allAddressAdapter.notifyDataSetChanged();
    }


    @OnClick(R.id.img_view_back)
    public void navBackImg() {
        backPress();
    }

    public void backPress() {
        super.onBackPressed();
        finish();
    }

    //edit_img
    @OnClick(R.id.edit_img)
    public void editAddress() {

        Intent intent = new Intent(SelectAddressActivity.this, EditAddressListActivity.class);
        startActivity(intent);
    }

    //add_address_btn
    @OnClick(R.id.add_address_btn)
    public void addAddressbtn() {

        Intent intent = new Intent(SelectAddressActivity.this, AddAddressActivity.class);
        startActivity(intent);
    }

}
