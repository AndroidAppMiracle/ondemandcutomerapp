package com.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.myapplication.R;
import com.myapplication.custome_controls.CustomeTextViewBold;
import com.myapplication.custome_controls.CustomeTextViewRegular;
import com.myapplication.utilities.RecyclerClick;

/**
 * Created by satoti.garg on 9/11/2017.
 */

public class AllAddressAdapter extends RecyclerView.Adapter<AllAddressAdapter.ViewHolder>{

    Context mContext;
    RecyclerClick recyclerClick;

    public AllAddressAdapter(Context mContext) {
        this.mContext = mContext;
        this.recyclerClick = recyclerClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,final int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_item, parent, false);
        return new AllAddressAdapter.ViewHolder(itemView, recyclerClick);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CustomeTextViewBold addressTitleTv;
        public CustomeTextViewRegular addressDetailTv;
        public CheckBox checkBox;
        public ImageView delImg;
        RecyclerClick recyclerClick;

        public ViewHolder(View view, RecyclerClick recyclerClick) {
            super(view);
            addressTitleTv = (CustomeTextViewBold) view.findViewById(R.id.address_title_tv);
            addressDetailTv= (CustomeTextViewRegular) view.findViewById(R.id.address_detail_tv);
            delImg=(ImageView)view.findViewById(R.id.edit_address_img);
            checkBox=(CheckBox)view.findViewById(R.id.check_address);
            delImg.setVisibility(View.VISIBLE);

        }

    }
}


