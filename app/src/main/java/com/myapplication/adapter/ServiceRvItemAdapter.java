package com.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.myapplication.R;
import com.myapplication.custome_controls.CustomeTextViewBold;
import com.myapplication.custome_controls.CustomeTextViewRegular;
import com.myapplication.utilities.RecyclerClick;

/**
 * Created by satoti.garg on 9/8/2017.
 */

public class ServiceRvItemAdapter extends RecyclerView.Adapter<ServiceRvItemAdapter.ServiceViewHolder> {
    Context mContext;
    RecyclerClick recyclerClick;

    public ServiceRvItemAdapter(Context mContext, RecyclerClick recyclerClick) {
        this.mContext = mContext;
        this.recyclerClick = recyclerClick;
    }

    @Override
    public ServiceViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.allservice_rv_item, parent, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerClick.productClick(v, viewType);
            }
        });
        return new ServiceViewHolder(itemView, recyclerClick);
    }

    @Override
    public void onBindViewHolder(ServiceViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ServiceViewHolder extends RecyclerView.ViewHolder {
        public CustomeTextViewBold businessNameTv;
        public CustomeTextViewRegular businessDetailTv;
        public ImageView imgRestaurant;
        public CheckBox checkServiceChk;
        RecyclerClick recyclerClick;

        public ServiceViewHolder(View view, RecyclerClick recyclerClick) {
            super(view);
            imgRestaurant = (ImageView) view.findViewById(R.id.del_service_img);
            businessNameTv = (CustomeTextViewBold) view.findViewById(R.id.business_name_tv);
            businessDetailTv = (CustomeTextViewRegular) view.findViewById(R.id.business_detail_tv);
            checkServiceChk = (CheckBox) view.findViewById(R.id.service_check);
            this.recyclerClick = recyclerClick;
        }

    }
}
