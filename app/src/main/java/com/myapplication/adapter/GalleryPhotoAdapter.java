package com.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.myapplication.R;
import com.myapplication.custome_controls.CustomeTextViewRegular;
import com.myapplication.utilities.RecyclerClick;

/**
 * Created by kanchan.salotra on 11/9/2017.
 */

public class GalleryPhotoAdapter extends RecyclerView.Adapter<GalleryPhotoAdapter.ServiceViewHolder> {
    Context mContext;
    RecyclerClick recyclerClick;

    public GalleryPhotoAdapter(Context mContext, RecyclerClick recyclerClick) {
        this.mContext = mContext;
        this.recyclerClick = recyclerClick;
    }

    @Override
    public ServiceViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_item, parent, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerClick.productClick(v, viewType);
            }
        });
        return new ServiceViewHolder(itemView, recyclerClick);
    }

    @Override
    public void onBindViewHolder(ServiceViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ServiceViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgRestaurant;
        RecyclerClick recyclerClick;

        public ServiceViewHolder(View view, RecyclerClick recyclerClick) {
            super(view);
            imgRestaurant = (ImageView) view.findViewById(R.id.img_vw_image);
            this.recyclerClick = recyclerClick;
        }

    }
}
