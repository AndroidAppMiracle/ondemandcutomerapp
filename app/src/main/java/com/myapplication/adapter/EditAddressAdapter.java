package com.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.myapplication.R;
import com.myapplication.custome_controls.CustomeTextViewBold;
import com.myapplication.custome_controls.CustomeTextViewRegular;
import com.myapplication.utilities.RecyclerClick;

/**
 * Created by satoti.garg on 9/11/2017.
 */

public class EditAddressAdapter extends RecyclerView.Adapter<EditAddressAdapter.ViewHolder>{

    Context mContext;
    RecyclerClick recyclerClick;

    public EditAddressAdapter(Context mContext,RecyclerClick recyclerClick) {
        this.mContext = mContext;
        this.recyclerClick = recyclerClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,final int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_item, parent, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerClick.productClick(v, viewType);
            }
        });
        return new EditAddressAdapter.ViewHolder(itemView, recyclerClick);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.editImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CustomeTextViewBold addressTitleTv;
        public CustomeTextViewRegular addressDetailTv;
        public ImageView editImg;
        public CheckBox checkAddress;

        RecyclerClick recyclerClick;

        public ViewHolder(View view, RecyclerClick recyclerClick) {
            super(view);
            addressTitleTv = (CustomeTextViewBold) view.findViewById(R.id.address_title_tv);
            addressDetailTv= (CustomeTextViewRegular) view.findViewById(R.id.address_detail_tv);
            editImg=(ImageView)view.findViewById(R.id.edit_address_img);
            checkAddress=(CheckBox)view.findViewById(R.id.check_address);
            checkAddress.setVisibility(View.GONE);
            this.recyclerClick = recyclerClick;
        }

    }
}


