package com.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.myapplication.R;
import com.myapplication.custome_controls.CustomeTextViewBold;
import com.myapplication.custome_controls.CustomeTextViewRegular;
import com.myapplication.utilities.RecyclerClick;

/**
 * Created by satoti.garg on 9/11/2017.
 */

public class OrderItemAdapter  extends RecyclerView.Adapter<OrderItemAdapter.ViewHolder>{

    Context mContext;
    RecyclerClick recyclerClick;

    public OrderItemAdapter(Context mContext) {
        this.mContext = mContext;
        this.recyclerClick = recyclerClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,final int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CustomeTextViewBold orderName;
        public CustomeTextViewRegular orderDesc;
        public ImageView delImg;
        CheckBox checkBox;
        RecyclerClick recyclerClick;

        public ViewHolder(View view) {
            super(view);
            orderName = (CustomeTextViewBold) view.findViewById(R.id.order_name_tv);
            orderDesc= (CustomeTextViewRegular) view.findViewById(R.id.order_detail_tv);
            delImg=(ImageView)view.findViewById(R.id.del_service_img);
            checkBox=(CheckBox)view.findViewById(R.id.service_check);
            checkBox.setVisibility(View.GONE);

        }

    }
}


