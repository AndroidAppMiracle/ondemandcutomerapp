package com.myapplication.custome_controls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by satoti.garg on 9/8/2017.
 */

public class CustomeTextViewRegular extends TextView {

    public CustomeTextViewRegular(Context context)
    {
        super(context); setFont();
    }

    public CustomeTextViewRegular(Context context,AttributeSet set)
    {
        super(context,set); setFont();
    }

    public CustomeTextViewRegular(Context context,AttributeSet set,int defaultStyle)
    {
        super(context,set,defaultStyle); setFont();
    }

    private void setFont() {

        Typeface typeface=Typeface.createFromAsset(getContext().getAssets(),"Muli-Light.ttf");
        setTypeface(typeface); //function used to set font

    }
}
