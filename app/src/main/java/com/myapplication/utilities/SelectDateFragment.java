package com.myapplication.utilities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/*import com.example.kanchans.hospitalappointmentapp.activities.BookAppointmentActivity;*/


/**
 * Created by kanchans on 8/29/2017.
 */

public class SelectDateFragment extends android.support.v4.app.DialogFragment implements DatePickerDialog.OnDateSetListener {

    TextView textView;
    int mm, yy, dd;

    public SelectDateFragment(TextView textView) {
        this.textView = textView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        populateSetDate(yy, mm , dd);
    }

    public void populateSetDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);

       /* BookAppointmentActivity.yy = year;
        BookAppointmentActivity.mm = month;
        BookAppointmentActivity.dd = day;*/

        String format = new SimpleDateFormat("MMM d, yyyy").format(cal.getTime());
        String formatforapi = new SimpleDateFormat("MM-dd-yyyy").format(cal.getTime());
   /*     BookAppointmentActivity.dateForApiST = format;
        BookAppointmentActivity.dateForApi=formatforapi;*/
        // BookAppointmentActivity BookAppointmentActivity=getGoalFragmentInstance();
        //  BookAppointmentActivity.getOrderHistoryArray();
        textView.setText(format);


    }


    public static void incDateByOne() {
//        BookAppointmentActivity.dd++;
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.YEAR, BookAppointmentActivity.yy);
//        cal.set(Calendar.DAY_OF_MONTH, BookAppointmentActivity.dd);
//        cal.set(Calendar.MONTH, BookAppointmentActivity.mm);
//
//        String format = new SimpleDateFormat("MMM d, yyyy").format(cal.getTime());
//        String formatforapi = new SimpleDateFormat("MM-dd-yyyy").format(cal.getTime());
//        BookAppointmentActivity.dateForApiST = /*String.valueOf(new StringBuilder().append(BookAppointmentActivity.yy).append("-").append(BookAppointmentActivity.mm).append("-").append(BookAppointmentActivity.dd).append(" "))*/format;
//        BookAppointmentActivity.dateForApi = /*String.valueOf(new StringBuilder().append(BookAppointmentActivity.yy).append("-").append(BookAppointmentActivity.mm).append("-").append(BookAppointmentActivity.dd).append(" "))*/formatforapi;
    }

    public static void decDateByOne() {

//        BookAppointmentActivity.dd--;
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.YEAR, BookAppointmentActivity.yy);
//        cal.set(Calendar.DAY_OF_MONTH, BookAppointmentActivity.dd);
//        cal.set(Calendar.MONTH, BookAppointmentActivity.mm);
//
//        String format = new SimpleDateFormat("MMM d, yyyy").format(cal.getTime());
//        String formatforapi = new SimpleDateFormat("MM-dd-yyyy").format(cal.getTime());
//
//        BookAppointmentActivity.dateForApiST = /*String.valueOf(new StringBuilder().append(BookAppointmentActivity.yy).append("-").append(BookAppointmentActivity.mm).append("-").append(BookAppointmentActivity.dd).append(" "))*/format;
//        BookAppointmentActivity.dateForApi = /*String.valueOf(new StringBuilder().append(BookAppointmentActivity.yy).append("-").append(BookAppointmentActivity.mm).append("-").append(BookAppointmentActivity.dd).append(" "))*/formatforapi;

    }
}