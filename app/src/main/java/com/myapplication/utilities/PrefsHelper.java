package com.myapplication.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kanchan on 3/15/2017.
 */

public class PrefsHelper {

    private static SharedPreferences preferences = null;
    public static final String DB_KEY_APP_START_TIME = "DB_KEY_APP_START_TIME";


    private static String PREFS_NAME = "hospital";
    public static String PHONE_NUMBER = "phone_number";
    public static String APOINTMENT_DATE = "apoint_date";
    public static String APOINTMENT_TIME = "apoint_timing";


    public PrefsHelper(Context context) {
        initSharedPrefs(context);
    }

  /*  public static String getStringForService(String value) {
        String returnValue = "";
        if (value.equals("timing")) {
            if (!HITTING_TIMING.equals("0")) {

                returnValue = HITTING_TIMING;
            } else {
                returnValue = "0";
            }

        } else if (value.equals("access_token")) {
            if (!ACCESS_TOKEN.equals("")) {

                returnValue = ACCESS_TOKEN;
            } else {
                returnValue = "0";
            }
        }
        return returnValue;
    }*/

    private void initSharedPrefs(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, 0);
    }
    public static String getSharedPreferences()
    {
    return PREFS_NAME;
    }

    public void saveStringData(String keyName, String val) {
        preferences.edit().putString(keyName, val).commit();
    }

    public String getStringValue(String key) {
        return preferences.getString(key, "");
    }

    public void saveIntData(String keyName, int val) {
        preferences.edit().putInt(keyName, val).commit();
    }

    public int getIntValue(String key) {
        return preferences.getInt(key, 0);
    }

    public void saveBooleanData(String keyName, boolean val) {
        preferences.edit().putBoolean(keyName, val).commit();
    }

    public boolean getBooleanValue(String key) {
        return preferences.getBoolean(key, false);
    }


    public void saveLongData(String keyName, long val) {
        preferences.edit().putLong(keyName, val).commit();
    }

    public long getLongData(String key) {
        return preferences.getLong(key, 1);
    }


    public static boolean clearPreference(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_NAME, 0);
        if (preferences != null) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            return editor.commit();
        }
        return false;
    }
}
